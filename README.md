# dwmblocks

Modular status bar for dwm written in c.

# Usage

To use dwmblocks first run 'make' and then install it with 'sudo make install'. After that you can put dwmblocks in your xinitrc or other startup script to have it start with dwm.

Activate blocks by signal with:

    kill -$((UPDATE_SIGNAL+34)) $(pidof dwmblocks)

# Modifying blocks

The statusbar is made from text output from commandline programs. Blocks are added and removed by editing the config.h header file.
