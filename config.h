// Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
    /*Icon*/ /*Command*/ /*Update Interval*/ /*Update Signal*/
    {"", "playerctl metadata -p spotifyd -f '{{ title   }} - {{ artist   }}'", 0, 10},
    {"Wi-Fi: ", "dwmblocks-wifi", 30, 0},
    {"Avail: ", "df -h / | awk 'NR==2 {print $4}'", 30, 0},
    {"Mem: ", "free -h | awk 'NR==2 {print $3}' | sed 's/i//'", 5, 0},
    {"CPU: ", "dwmblocks-cpu", 5, 0},
    {"", "curl -sL wttr.in/?format=%t+%C", 7200, 0},
    {"", "date '+%R %B %d, %Y'", 5, 0},
};

// Sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
